#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

CLUSTER="${1:-}"
if [ "$CLUSTER" != "dev" ] && [ "$CLUSTER" != "prod-01" ] && [ "$CLUSTER" != "prod-02" ]; then
  echo "Please run 'restore [dev|prod-01|prod-02] ...'"
  exit 1
fi

if [ ! -f "./kubeconfig-orka-$CLUSTER" ]; then
  echo "Please get your kubeconfig for $CLUSTER saved to ./kubeconfig-orka-$CLUSTER"
  exit 1
fi

kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/gcs-runners-cache-credentials.yaml"
kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/runner-gitlab-org-orka.yaml"
./deploy.sh "$CLUSTER" gitlab-org-orka

if [ "$CLUSTER" == "dev" ]; then
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "manifests/metrics-dev.yaml"
fi

if [ "$CLUSTER" == "prod-01" ] || [ "$CLUSTER" == "prod-02" ]; then
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/orka-beta.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/runner-orka-beta.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/orka-canary.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/runner-orka-canary.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "backup/$CLUSTER/gitlab-allowlist-token.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "manifests/allowlist.pvc.yml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "manifests/allowlist.cron.yml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" apply -f "manifests/metrics-$CLUSTER.yaml"
  ./deploy.sh "$CLUSTER" canary
  ./deploy.sh "$CLUSTER" beta
fi

echo "Restore completed!"
