#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

CLUSTER=${1:-}
case $CLUSTER in dev|prod-01|prod-02);;
  *) echo "Usage: $0 [dev|prod-01|prod-02]"; exit 1;;
esac

sudo --validate --prompt="Enter sudo password: "

if ! op -v &> /dev/null; then echo '1password-cli not found. Install with "brew install 1password-cli"'; exit 1; fi
if ! sudo openconnect -V &> /dev/null; then echo 'openconnect not found. Install with "brew install openconnect"'; exit 1; fi

eval "$(op signin --account gitlab.1password.com)"

ORKA_VPN_USER="$(op item get --vault Verify --account gitlab --fields label="username" "Orka VPN - $CLUSTER")"
ORKA_VPN_PASS="$(op item get --vault Verify --account gitlab --fields label="password" "Orka VPN - $CLUSTER")"
ORKA_VPN_CERT="$(op item get --vault Verify --account gitlab --fields label="Cert" "Orka VPN - $CLUSTER")"
ORKA_VPN_IP="$(op item get --vault Verify --account gitlab "Orka VPN - $CLUSTER" --format json | jq -r .urls[0].href)"

echo "Connecting to the $CLUSTER VPN..."
echo "$ORKA_VPN_PASS" | sudo openconnect "$ORKA_VPN_IP" --protocol=anyconnect --user="$ORKA_VPN_USER" --servercert "$ORKA_VPN_CERT" --passwd-on-stdin
