#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

CLUSTER=${1:-}
case $CLUSTER in dev|prod-01|prod-02);;
  *) echo "Usage: $0 [dev|prod-01|prod-02]"; exit 1;;
esac

sudo --validate --prompt="Enter sudo password: "

if ! op -v &> /dev/null; then echo '1password-cli not found. Install with "brew install 1password-cli"'; exit 1; fi
if ! sshpass -V &> /dev/null; then echo 'sshpass not found. Install with "brew install gitlab/shared-runners/sshpass"'; exit 1; fi

eval "$(op signin --account gitlab.1password.com)"

ORKA_VPN_USER="$(op item get --vault Verify --account gitlab --fields label="username" "Orka VPN - $CLUSTER")"
ORKA_VPN_PASS="$(op item get --vault Verify --account gitlab --fields label="password" "Orka VPN - $CLUSTER")"
ORKA_VPN_CERT="$(op item get --vault Verify --account gitlab --fields label="Cert" "Orka VPN - $CLUSTER")"

echo "Connecting to the $CLUSTER ASA over SSH..."
sshpass -p "$ORKA_VPN_PASS" \
  ssh -T \
  -o ConnectTimeout=5 -o ConnectionAttempts=1 \
  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
  -o KexAlgorithms=+diffie-hellman-group14-sha1 \
  "$ORKA_VPN_USER"@"$ORKA_ASA_IP"
