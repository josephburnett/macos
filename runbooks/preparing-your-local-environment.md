# Preparing your local environment

## Dependencies

The following dependencies are needed to access the environment

```shell
brew install gitlab/shared-runners/sshpass homebrew/cask/1password-cli homebrew/cask/orka homebrew/cask/docker openconnect kubernetes-cli jq gnu-sed helm k9s
```

## Cisco VPN

The Orka cluster and API are behind a VPN. To access it:

1. Ensure `op` can access your company 1password account by running at least once:

    ```shell
    op signin gitlab.1password.com YOU@gitlab.com
    ```

1. Start the VPN connection in a separate shell with the following script

    ```shell
    ./vpn.sh dev # or ./vpn.sh prod-XX ...
    ```

## Orka CLI

1. Go through the [Orka CLI Quick Start](https://orkadocs.macstadium.com/docs/quick-start).
   You can get the information related to the IP plan (VPN endpoint and API URL) and license key
   from 1password, in the `Orka VPN` and `Orka License` entries.
   You should end up with your own orka user `you@gitlab.com` and be able to use the CLI
   to list and create VMs as shown in the tutorial.

1. Make sure your orka user is in the `gitlab` group. This will allow you to launch VMs
only on nodes grouped into the `gitlab` group.

    ```shell
    orka user group -g gitlab -e YOU@gitlab.com
    ```

## Kubernetes API

1. Go through the [Tapping into Kubernetes](https://orkadocs.macstadium.com/docs/tapping-into-kubernetes)
   tutorial. You should end up with your `kubeconfig-orka` file stored locally and the ability to run `kubectl get pods`.

1. Make sure to export your kubeconfig location

   ```shell
   export KUBECONFIG=$(pwd)/kubeconfig-orka-dev # or kubeconfig-orka-prod-XX
   ```

## Orka wrapper

1. Add the following to your `shellrc`:

   ```shell
   alias orka=$(path to repo)/orka-wrapper.sh
   ```

1. Reconfigure your CLI with the new wrapper.

   ```shell
   orka dev config # or orka prod-XX ...
   orka dev login
   ```
