# Add a new project to the Orka canary runner

## Execution

1. Go into the `orka` project > Settings > CI/CD > Runners

1. Find the `macos-shared-runners-canary` runner, and edit it.

1. Uncheck `When a runner is locked, it cannot be assigned to other projects`

1. Go into the project you want to add > Settings > CI/CD > Runners

1. Under `Other available runners`, find the `macos-shared-runners-canary` and click `Enable for this project`

1. Edit the runner again and check `When a runner is locked, it cannot be assigned to other projects` again.
