# Add a new cluster

Let's say you want to add a new cluster next to `dev` and `prod-01`, let's call it `prod-02`

## Preparation

Go through the runbook [Preparing your local environment](./preparing-your-local-environment.md)

## Execution

1. Create a new shared ci user: `orka prod-02 user create`. Set the email `ci@gitlab.com` and the password associated to that user in 1password.

1. Create a kubernetes account with `orka prod-02 kube create --account USERNAME -y`. Make sure to rename the `./kubeconfig-orka` to `./kubeconfig-orka-prod-02`.

1. Go through the runbook [Add a new runner environment](./add-a-new-runner-environment.md) for the `beta` and `canary` environments.
